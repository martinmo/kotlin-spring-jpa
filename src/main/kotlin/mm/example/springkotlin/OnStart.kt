package mm.example.springkotlin

import mm.example.springkotlin.domain.entity.Entity
import mm.example.springkotlin.domain.entity.EntityRepo
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
class OnStart(private val entityRepo: EntityRepo) {

    @PostConstruct
    fun onApplicationStart() {
        val entity = Entity(message = "Hello")
        entityRepo.save(entity)

        println(entityRepo.findAll())
    }

}