package mm.example.springkotlin.domain

import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
abstract class VersionedEntity(
        reference: Reference?
) {
    @Id @GeneratedValue val id: Long = reference?.id ?: -1
    @Version val version: Long = reference?.version ?: -1

    data class Reference(val id: Long, val version: Long) {
        override fun toString(): String = "$id-$version"

        companion object {
            operator fun invoke(string: String): Reference {
                val (id: Long, version: Long) = string.split("-").map(String::toLong)
                return Reference(id, version)
            }

            fun VersionedEntity.reference(): Reference = Reference(id = id, version = version)
        }
    }
}