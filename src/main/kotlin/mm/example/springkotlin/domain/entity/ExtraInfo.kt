package mm.example.springkotlin.domain.entity

import javax.persistence.Column
import javax.persistence.Embeddable


@Embeddable
data class ExtraInfo(
        @Column(nullable = false)
        val info: String = ""
)