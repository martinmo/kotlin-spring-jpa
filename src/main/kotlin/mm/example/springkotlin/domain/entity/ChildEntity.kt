package mm.example.springkotlin.domain.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class ChildEntity(
        val name: String,
        @Id @GeneratedValue val id: Long = -1
)