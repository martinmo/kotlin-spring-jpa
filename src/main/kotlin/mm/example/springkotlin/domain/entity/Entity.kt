package mm.example.springkotlin.domain.entity

import mm.example.springkotlin.domain.VersionedEntity
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.ZonedDateTime
import javax.persistence.*
import javax.persistence.Entity


@Entity
@EntityListeners(AuditingEntityListener::class)
class Entity(
        @Column(nullable = false)
        var message: String,

        @OneToMany(cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
        private val children: MutableList<ChildEntity> = mutableListOf(),

        @Embedded
        var extra: ExtraInfo = ExtraInfo(),

        @Column(nullable = false)
        @CreatedDate val created: ZonedDateTime = ZonedDateTime.now(),

        @Column(nullable = false)
        @LastModifiedDate val modified: ZonedDateTime = ZonedDateTime.now(),

        reference: Reference? = null
) : VersionedEntity(reference) {

    fun children(): List<String> = children.map { it.name }
    fun replaceChildren(newChildren: List<String>) {
        children.clear()
        children.addAll(newChildren.map { ChildEntity(it) })
    }
}


