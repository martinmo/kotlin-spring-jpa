package mm.example.springkotlin.domain.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface EntityRepo : CrudRepository<Entity, Long>