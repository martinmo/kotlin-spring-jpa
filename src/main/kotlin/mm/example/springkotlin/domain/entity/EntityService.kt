package mm.example.springkotlin.domain.entity

import mm.example.springkotlin.domain.VersionedEntity
import org.springframework.stereotype.Service
import javax.persistence.OptimisticLockException
import javax.transaction.Transactional

@Service
@Transactional
class EntityService(private val repo: EntityRepo) {

    fun create(message: String, children: List<String>): Entity {
        return repo.save(Entity(message = message, children = children.map { ChildEntity(it) }.toMutableList()))
    }

    fun <T> update(reference: VersionedEntity.Reference, consumer: (Entity) -> T): T {
        val entity = repo.findOne(reference.id)

        if (entity.version != reference.version) {
            throw OptimisticLockException("outdated version")
        }

        val result = consumer.invoke(entity)
        repo.save(entity)
        return result
    }

    fun all(): List<Entity> = repo.findAll().toList()

    fun get(id: Long): Entity = repo.findOne(id) ?: throw IllegalArgumentException("not found")
}
