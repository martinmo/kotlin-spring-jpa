package mm.example.springkotlin.api.entity

import mm.example.springkotlin.api.Redirect
import mm.example.springkotlin.domain.VersionedEntity
import mm.example.springkotlin.domain.VersionedEntity.Reference.Companion.reference
import mm.example.springkotlin.domain.entity.Entity
import mm.example.springkotlin.domain.entity.EntityService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController()
@RequestMapping(path = arrayOf("/entities"))
class EntityResource(private val entityService: EntityService) {

    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: Long): Dto {
        return entityService.get(id).toDto()
    }

    @GetMapping()
    fun list(): List<Dto> = entityService.all().map(Entity::toDto)

    @PostMapping
    fun create(@RequestBody dto: Dto): ResponseEntity<Any> {
        val saved = entityService.create(dto.message, dto.children)
        return Redirect.redirect("entities", saved.reference())
    }

    @PutMapping("/{ref}")
    fun update(@PathVariable("ref") ref: String, @RequestBody dto: Dto): ResponseEntity<Any> {
        val reference = VersionedEntity.Reference(ref)

        entityService.update(reference, { entity ->
            entity.message = dto.message
            entity.replaceChildren(dto.children)
        })
        return Redirect.redirect("entities", reference)
    }
}

data class Dto(val message: String, val children: List<String>, val ref: String? = null)

fun Entity.toDto(): Dto = Dto(
        ref = reference().toString(),
        message = message,
        children = children()
)
