package mm.example.springkotlin.api

import mm.example.springkotlin.domain.VersionedEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.net.URI

object Redirect {
    fun redirect(pathWithoutSlash: String, id: VersionedEntity.Reference? = null): ResponseEntity<Any> {
        val headers = HttpHeaders()
        val path = if (id != null) "/$pathWithoutSlash/" + id.id else "/$pathWithoutSlash"
        headers.location = URI.create(path)
        return ResponseEntity<Any>(headers, HttpStatus.SEE_OTHER)
    }
}
