package mm.example.springkotlin.api.entity

import io.restassured.RestAssured
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import mm.example.springkotlin.domain.entity.ChildEntity
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.containsString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*
import javax.persistence.EntityManager


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EntityResourceTest {

    @LocalServerPort lateinit var port: Integer

    @Autowired lateinit var em: EntityManager

    @Before fun init() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
        RestAssured.port = port.toInt()
    }

    @Test
    fun canList() {
        val list: List<Dto> = givenRequest().get("/entities")
                .then().assertThat()
                .extract().`as`(Array<Dto>::class.java).toList()
        assertThat(list).isNotEmpty
    }

    @Test
    fun canCreate() {
        val created = aEntity(children = listOf("child1", "child2"))

        val loaded = listAll().find { it.ref == created.ref }!!

        assertThat(loaded.message).isEqualTo(created.message)
        assertThat(loaded.children).containsExactly("child1", "child2")
    }

    @Test
    fun canUpdate() {
        val created = aEntity(children = listOf("oldChild"))

        val childEntitiesBefore = loadChildEntities().size

        val updated = givenRequest()
                .body("""{
                        "message":"messageForUpdate",
                        "children":["newChild"]
                        }""")
                .contentType(ContentType.JSON)
                .put("/entities/${created.ref}")
                .then().assertThat()
                .extract().`as`(Dto::class.java)

        assertThat(updated.message).isEqualTo("messageForUpdate")
        assertThat(updated.children).containsExactly("newChild")

        assertThat(loadChildEntities().size).isEqualTo(childEntitiesBefore)
    }

    private fun loadChildEntities(): List<ChildEntity> {
        val all = em.createQuery("select c from ChildEntity c", ChildEntity::class.java).resultList
        return all
    }

    @Test
    fun canOptimisticLockingWorks() {
        val created = aEntity()

        val updatedRef = givenRequest()
                .body("""{
                        "message":"message2",
                        "children":["newChild"]
                        }""")
                .contentType(ContentType.JSON)
                .put("/entities/${created.ref}")
                .then().assertThat()
                .statusCode(200)
                .extract().jsonPath().get<String>("ref")


        givenRequest()
                .body("""{
                        "message":"message3",
                        "children":["newChild"]
                        }""")
                .contentType(ContentType.JSON)
                .put("/entities/${created.ref}")
                .then().assertThat()
                .statusCode(500)
                .body(containsString("Optimistic"))

        val foundInList = listAll().find { it.ref == updatedRef }
        assertThat(foundInList).isNotNull()
        assertThat(foundInList!!.message).isEqualTo("message2")
    }

    private fun givenRequest(): RequestSpecification {
        return given()
                .contentType(ContentType.JSON)
                .log().all()
                .response().log().all()
                .request()
    }

    private fun aEntity(
            message: String = UUID.randomUUID().toString(),
            children: List<String> = emptyList()
    ): Dto {

        val childrenJson = children.map { "\"$it\"" }.joinToString(",")

        val created = givenRequest()
                .body("""{
                            "message":"$message",
                            "children": [$childrenJson]
                      }""")
                .contentType(ContentType.JSON)
                .post("/entities")
                .then().assertThat()
                .statusCode(200)
                .extract().`as`(Dto::class.java)
        return created
    }

    private fun listAll() = givenRequest().get("/entities").`as`(Array<Dto>::class.java).toList()
}
