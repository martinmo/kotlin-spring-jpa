package mm.example.springkotlin.domain.entity

import mm.example.springkotlin.domain.VersionedEntity.Reference.Companion.reference
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.junit4.SpringRunner
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager

@RunWith(SpringRunner::class)
@DataJpaTest()
@Import(EntityService::class)
class EntityServiceTest {

    @Autowired lateinit var service: EntityService
    @Autowired lateinit var em: EntityManager

    @Test
    fun canCreate() {
        service.create("e1", listOf("child1"))

        val all = service.all()

        assertThat(all).hasSize(1)

        val loaded = all.first()

        assertThat(loaded.id).isNotEqualTo(-1)
        assertThat(loaded.message).isEqualTo("e1")

        assertThat(loaded.children()).hasSize(1)
        assertThat(loaded.children().first()).isEqualTo("child1")
    }

    @Test
    fun onUpdateOrphanChildrenAreRemoved() {
        val saved = anEntity(children = listOf("child1", "child2"))

        assertThat(loadChildEntities()).hasSize(2)

        service.update(saved.reference(), {
            it.replaceChildren(listOf("child1"))
        })

        em.flush()

        assertThat(loadChildEntities()).hasSize(1)
    }

    @Test
    fun canUseEmbeddable() {
        val saved: Entity = service.create("e1", listOf())

        service.update(saved.reference(), {
            it.extra = ExtraInfo("new Extra Info")
        })

        assertThat(service.all().first().extra.info).isEqualTo("new Extra Info")
    }

    @Test
    fun auditWorks() {
        val saved = service.create("e1", listOf())

        Thread.sleep(500)
        service.update(saved.reference(), {
            it.message = "changed"
        })

        em.flush()

        val updated = service.get(saved.id)

        assertThat(updated.message).isEqualTo("changed")
        assertThat(updated.modified).isGreaterThanOrEqualTo(updated.created.plus(500, ChronoUnit.MILLIS))
        assertThat(saved.created).isEqualTo(updated.created)
    }

    private fun loadChildEntities() = em.createQuery("select c from ChildEntity c", ChildEntity::class.java)
            .resultList.toList()

    private fun anEntity(name: String = "aEntity", children: List<String> = emptyList()): Entity =
            service.create(name, children)
}